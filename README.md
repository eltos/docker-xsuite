# Xsuite Docker Image

Docker image with [Xsuite](https://www.classe.cornell.edu/bmad/overview.html).

GPU support is enabled via [PyOpenCL](https://documen.tician.de/pyopencl) and will use any OpenCL drivers registered in `/etc/OpenCL/vendors/`.

## Usage

See **[Registry](https://gitlab.com/eltos/docker-xsuite/container_registry)** for all builds

```yml
image: registry.gitlab.com/eltos/docker-xsuite
```

**Docker:**
```bash
docker run -it registry.gitlab.com/eltos/docker-xsuite
```

**Singularity:**
```bash
singularity pull xsuite.sif docker://registry.gitlab.com/eltos/docker-xsuite:latest
singularity exec xsuite.sif bash
```
