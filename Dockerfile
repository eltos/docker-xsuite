# Dockerfile for Xsuite with GPU support
# https://xsuite.readthedocs.io/en/latest/installation.html

FROM continuumio/miniconda3

SHELL ["/bin/bash", "-c"]


# GPU support
##############

RUN apt update && apt install -y \
  clinfo \
  build-essential \
  # cleanup
  #&& apt --purge remove -y .\*-doc$ \
  #&& apt clean -y \
  #&& rm -rf /var/lib/apt/lists/* \
  && echo

RUN conda install -y -c conda-forge \
  pyopencl ocl-icd-system \
  cython clfft \
  && CLFFT=$(find $(dirname $(dirname $(type -P conda)))/pkgs -name "clfft*" -type d) \
  && git clone https://github.com/geggo/gpyfft \
  && pip install --global-option=build_ext --global-option="-I$CLFFT/include" --global-option="-L$CLFFT/lib" gpyfft/ \
  # cleanup
  #&& rm -drf gpyfft \
  #&& find /opt/conda/ -follow -type f -name '*.a' -delete \
  #&& find /opt/conda/ -follow -type f -name '*.pyc' -delete \
  #&& find /opt/conda/ -follow -type f -name '*.js.map' -delete \
  #&& /opt/conda/bin/conda clean -y --all --force-pkgs-dirs \
  && echo


# Xsuite installation
######################

RUN pip install \
  xsuite \
  # cleanup
  #&& pip cache purge \
  && echo


# Python packages
##################

RUN pip install \
  numpy scipy pandas pint matplotlib dill \
  # cleanup
  #&& pip cache purge \
  && echo

